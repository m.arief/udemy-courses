# udemy-courses

Based on the [Udemy Blog](https://about.udemy.com/?locale=en-us), Udemy is the leading global marketplace for learning and instruction. By connecting students all over the world to the best instructors, Udemy is helping individuals reach their goals and pursue their dreams.

In this project I start to analyze the Udemy courses based on the dataset I got. My project used python and Jupyter Notebook as a tool. This project will divide into 4 section:
1. **Import library and dataset.** At the first section, I import the needed library and read the dataset. If you want to get the dataset, here I attach the link. [Link Dataset Udemy Courses](https://www.kaggle.com/andrewmvd/udemy-courses)
2. **EDA (Exploratory Data Analysis)**. In this section, I will change the data types, handling the missing data and drop some not important data and add some value that I think it will be important
3. **Analyze.** In this section, I will visualize the data and get some insight. Check the Dashboard [here](https://public.tableau.com/profile/m.arief#!/vizhome/UdemyCourses_15941734148470/UdemyCourses)
4. **Conclusion**. Based on the insight I got when I analyze I collect and write the conclusion in the end.
 



